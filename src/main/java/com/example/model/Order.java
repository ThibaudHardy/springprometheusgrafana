package com.example.model;

import java.util.Random;
import java.util.UUID;

public class Order {

  private String id;
  private Double number;
  private String name;
  private String surname;
  private String product;
  private String adress;

  public Order(String name, String surname, String product, String adress) {

    this.name = name;
    this.surname = surname;
    this.product = product;
    this.adress = adress;
    this.id = UUID.randomUUID().toString();
  }

  public String getId() {
      return this.id;
  }

  public Double getNumber() {
      return this.number;
  }

  public void setNumber(Double number) {
      this.number = number;
  }

  public String getName() {
      return this.name;
  }

  public void setName(String name) {
      this.name = name;
  }

  public String getSurname() {
      return this.surname;
  }

  public void setSurname(String surname) {
      this.surname = surname;
  }

  public String getProduct() {
      return this.product;
  }

  public void setProduct(String product) {
      this.product = product;
  }

  public String getAdress() {
      return this.adress;
  }

  public void setAdress(String adress) {
      this.adress = adress;
  }
}