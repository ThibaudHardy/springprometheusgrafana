package com.example.demo;

import org.springframework.boot.SpringApplication;

import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.model.Order;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Counter;


@RestController
public class OrderController {

    private Counter orderCounter;

    public void Scheduler(MeterRegistry meterRegistry) {
        orderCounter = meterRegistry.counter("custom_counter");
    }

    @PostMapping(path = "/order")
    public ResponseEntity<Order> order(
        @RequestBody Order newOrder
    ) {
        //orderCounter.increment();
        //newOrder.setNumber(orderCounter.count());
        return new ResponseEntity<>(newOrder, HttpStatus.CREATED);
    }
}