Feature: Post Order

    Background: 
        * url 'http://localhost:8080'

    Scenario: Post an Order
        Given path '/order'
        And request { name: 'testName', surname: 'testSurname', adress: 'testAdress' }
        When method POST
        Then status 201
        And match $ == {"number":#null,"product":#null,"surname":testSurname,"name":"testName","adress":"testAdress","id":#ignore}