package feature.order;

import com.intuit.karate.junit5.Karate;

class OrderIT {
    @Karate.Test
    Karate testOrder() {
        return Karate.run("PostOrder").relativeTo(getClass());
    }
}   